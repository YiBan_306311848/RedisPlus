package com.maxbill.fxui.body;

import com.maxbill.base.bean.ConfNode;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import static com.maxbill.fxui.body.BodyWindow.getTabPane;
import static com.maxbill.fxui.util.CommonConstant.TEXT_TAB_CONF;
import static com.maxbill.util.RedisUtil.getConfList;

/**
 * 配置窗口
 *
 * @author MaxBill
 * @date 2019/07/10
 */
public class ConfWindow {

    private static Tab confTab = null;

    public static void initConfTab() {
        if (null == confTab) {
            confTab = new Tab(TEXT_TAB_CONF);
            getTabPane().getTabs().add(confTab);
        }
        getTabPane().getSelectionModel().select(confTab);

        ObservableList<ConfNode> data = FXCollections.observableArrayList(getConfList());
        TableView<ConfNode> tableView = new TableView<>(data);
        TableColumn<ConfNode, String> keyCol = new TableColumn<>("配置");
        TableColumn<ConfNode, String> valCol = new TableColumn<>("数据");
        TableColumn<ConfNode, String> txtCol = new TableColumn<>("描述");

        keyCol.setSortable(false);
        valCol.setSortable(false);
        txtCol.setSortable(false);

        tableView.getColumns().add(keyCol);
        tableView.getColumns().add(valCol);
        tableView.getColumns().add(txtCol);
        tableView.setItems(data);
        keyCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((ConfNode) node).getKey());
        });
        valCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((ConfNode) node).getVal());
        });
        txtCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((ConfNode) node).getTxt());
        });
        confTab.setContent(tableView);
        confTab.setOnClosed(event -> confTab = null);
    }

}
